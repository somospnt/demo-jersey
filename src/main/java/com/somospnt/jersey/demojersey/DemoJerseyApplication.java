package com.somospnt.jersey.demojersey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJerseyApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoJerseyApplication.class, args);
    }
}
