package com.somospnt.jersey.demojersey.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Persona {
    
    private Long id;
    private String nombre;
    
}
