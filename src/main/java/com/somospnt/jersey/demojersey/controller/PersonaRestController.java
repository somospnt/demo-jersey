package com.somospnt.jersey.demojersey.controller;

import com.somospnt.jersey.demojersey.domain.Persona;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.stereotype.Component;

@Path("/personas")
@Component
public class PersonaRestController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> buscarTodas() {
        return Arrays.asList(new Persona(1L, "coco"), new Persona(2L, "pepe"));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Persona buscraPorId(@PathParam("id") Long id) {
        return new Persona(id, "coco");
    }

    @POST
    @Consumes("application/json")
    public void alta(Persona persona) {
        System.out.println("####################### " + persona);
    }
    
}
